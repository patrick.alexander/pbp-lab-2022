from django.urls import path

from lab_2.views import index
from .views import index, xml_watchlist, json_watchlist

urlpatterns = [
    path('', index, name='index'),
    path('xml/', xml_watchlist),
    path('json/', json_watchlist),
    
]
