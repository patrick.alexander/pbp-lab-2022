from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from datetime import datetime, date
from lab_3.models import MyWatchlist

# Create your views here.
def index(request):
    movie = MyWatchlist.objects.all() # TODO Implement this
    response = {'MyWatchlist': movie}
    return render(request, 'lab3.html', response)

def xml_watchlist(request):
    data = serializers.serialize('xml', MyWatchlist.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json_watchlist(request):
    data = serializers.serialize('json', MyWatchlist.objects.all())
    return HttpResponse(data, content_type="application/json")