from django.urls import path

from .views import index, tambah_tugas

urlpatterns = [
    path('', index, name='index'),
    path('newform/', tambah_tugas),
]
