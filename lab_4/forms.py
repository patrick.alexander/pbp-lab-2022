from dataclasses import field
from tkinter import Widget
from urllib import request
from django import forms
from lab_2.models import TrackerTugas

class TugasForm(forms.ModelForm):
    class Meta:
        model = TrackerTugas
        fields = "__all__"
        widgets = {
            'course': forms.TextInput(),
            'detail': forms.Textarea(),
            'deadline': forms.DateInput(),
            
            

        }
