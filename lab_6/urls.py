from django.urls import path
from django.contrib.auth import views as auth_views
from .views import index
from django.conf.urls.static import static, settings

urlpatterns = [
    path('', index, name='index'),
]


