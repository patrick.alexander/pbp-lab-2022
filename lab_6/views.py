from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from datetime import datetime, date
from lab_3.models import MyWatchlist

# Create your views here.
def index(request):
    movie = MyWatchlist.objects.all() # TODO Implement this
    response = {'MyWatchlist': movie}
    return render(request, 'lab6_index.html', response)
