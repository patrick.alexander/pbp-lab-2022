import 'package:flutter/foundation.dart';

enum Rating {
  Fresh,
  Average,
  Rotten,
}

enum Language {
  English,
  Indonesian,
  Korean,
  Spanish,
}

class Meal {
  final String id;
  final List<String> categories;
  final String title;
  final String imageUrl;
  final String description;
  final String review ;
  final int duration;
  final Rating rating;
  final Language language;
  final bool isFresh;
  final bool isEnglish;


  const Meal({
    @required this.id,
    @required this.categories,
    @required this.title,
    @required this.imageUrl,
    @required this.description,
    @required this.review,
    @required this.duration,
    @required this.rating,
    @required this.language,
    @required this.isFresh,
    @required this.isEnglish,

  });
}
