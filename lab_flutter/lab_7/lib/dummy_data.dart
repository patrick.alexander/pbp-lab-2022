import 'package:flutter/material.dart';

import './models/category.dart';
import './models/meal.dart';

const DUMMY_CATEGORIES = const [
  Category(
    id: 'c1',
    title: 'Horror',
    color: Colors.purple,
  ),
  Category(
    id: 'c2',
    title: 'Action',
    color: Colors.red,
  ),
  Category(
    id: 'c3',
    title: 'Comedy',
    color: Colors.orange,
  ),
  Category(
    id: 'c4',
    title: 'Mystery',
    color: Colors.amber,
  ),
  Category(
    id: 'c5',
    title: 'Animation',
    color: Colors.blue,
  ),
  Category(
    id: 'c6',
    title: 'Romance',
    color: Colors.green,
  ),
  Category(
    id: 'c7',
    title: 'Scifi',
    color: Colors.lightBlue,
  ),
  
];

const DUMMY_MEALS = const [
  Meal(
    id: 'm1',
    categories: [
      'c1',
      
    ],
    title: 'Insidious',
    rating: Rating.Fresh,
    language: Language.English,
    imageUrl:
        'https://img.okezone.com//content/2015/03/18/206/1120734/insidious-3-dijamin-lebih-menyeramkan-VR1wKgrrku.jpg',
    duration: 103,
    description: 
      "Josh and Renai move to a new house, seeking a fresh start. However, when their son, Dalton, mysteriously falls into a coma, paranormal events start occurring in the house."
    ,

    review: 
      "Director James Wan here proves himself very adept at building dramatic tension and making you anxious about the things going on in your peripheral vision."
    ,
    
    isEnglish: true,
    isFresh: true,
    
  ),
  Meal(
    id: 'm2',
    categories: [
      'c1',
      
    ],
    title: 'The Curse of La Llorona',
    rating: Rating.Rotten,
    language: Language.English,
    imageUrl:
        'https://i.pinimg.com/originals/d2/56/28/d25628a7c998abe8a79412b7e7839699.jpg',
    duration: 93,
    description: 
      "When Patricia is found endangering the lives of her sons, Anna's puts her behind bars. However, when Annas own children are endangered, she suspects that there is more than human involvement at play."
    ,
    review: 
      "The franchise continues, whether you want it to or not, because each entry is relatively inexpensive, and because they earn hundreds of millions at the box-office. Although you might need a flowchart to determine how they all fit together"
    ,
    
    isEnglish: true,
    isFresh: false,
    
  ),
  Meal(
    id: 'm3',
    categories: [
      'c2',
    ],
    title: 'Avengers: Endgame',
    rating: Rating.Fresh,
    language: Language.English,
    imageUrl:
        'https://cdn.pixabay.com/photo/2018/07/11/21/51/toast-3532016_1280.jpg',
    duration: 181,
    description:
      "After Thanos, an intergalactic warlord, disintegrates half of the universe, the Avengers must reunite and assemble again to reinvigorate their trounced allies and restore balance."
    ,
    review: 
      "Avengers: Endgame broke me, put me back together, and decided to cut me again in one of the most impactful cinematic experiences of all time."
    ,
    isEnglish: true,
    isFresh: true,
  ),

];
