from django.urls import path
from django.contrib.auth import views as auth_views
from .views import index, mywatchlist, add_watchlist
from django.conf.urls.static import static, settings

urlpatterns = [
    path('', index, name='index'),
    path('watchlist/', mywatchlist),
    path('addwatchlist/', add_watchlist),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
