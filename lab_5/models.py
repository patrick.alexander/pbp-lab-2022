from django.db import models

# Create your models here.
class MyWatchlist(models.Model):
    watched = models.BooleanField(default=False, blank=True)
    title = models.CharField(max_length=100, blank=True)
    rating = models.FloatField(blank=True)
    release_date = models.DateField(blank=True)
    review = models.TextField(blank=True)
    image_content = models.ImageField(upload_to='lab5-images', blank=True)

