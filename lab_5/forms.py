from dataclasses import field
from tkinter import Widget
from urllib import request
from django import forms
from lab_5.models import MyWatchlist

class WatchlistForm(forms.ModelForm):
    class Meta:
        model = MyWatchlist
        fields = "__all__"
        widgets = {
            'watched': forms.CheckboxInput(),
            'Title': forms.TextInput(attrs={"placeholder":"Masukan Title"}),
            'Rating': forms.NumberInput(),
            'Release date': forms.DateInput(),
            'Review': forms.Textarea(attrs={"rows": 3}),
            
            

        }
