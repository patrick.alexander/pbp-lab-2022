from django.shortcuts import render
from lab_5.models import MyWatchlist
from django.contrib.auth.decorators import login_required
from .forms import WatchlistForm
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    movie = MyWatchlist.objects.all() # TODO Implement this
    response = {'MyWatchlist': movie}
    return render(request, 'lab5_index.html', response)

def mywatchlist(request):
    movie = MyWatchlist.objects.all() # TODO Implement this
    response = {'MyWatchlist': movie}
    return render(request, 'lab5_watch_list.html', response)

@login_required(login_url='/admin/login/')
def add_watchlist(request):
    print("a")
    form = WatchlistForm(request.POST , request.FILES)
    print(form.is_valid())
    # check if form data is valid
    if request.method== "POST":
        print("b")
        if form.is_valid():
            print("c")
        # save the form data to model
            form.save()
            #return HttpResponseRedirect('/lab-5/watchlist') 
    response = {'form': form}
    return render(request, "lab5_form.html", response)


