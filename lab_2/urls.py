from django.urls import path
from .views import index
from .views import list_tugas

urlpatterns = [
    path('', index, name='index'),
    # TODO Add 'tugas' path using list_tugas Views
    path('list-tugas/', list_tugas),
    
]
